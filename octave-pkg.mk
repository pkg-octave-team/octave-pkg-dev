# -*- makefile -*-
# octave-pkg.mk: dh-based debian/rules scrap for building/installing
#                octave-forge pkg add-ons
# This file is part of the octave-pkg-dev Debian package

# Copyright (c) 2008  Olafur Jens Sigurðsson <ojsbug@gmail.com>
# Copyright (c) 2008, 2009, 2012, 2017  Rafael Laboissiere <rafael@laboissiere.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

export package = $(shell awk '/^Package:/ {print $$2; exit 0}' debian/control)
export debpkg = debian/$(package)
export shrpkg = /usr/share/octave/debian
# The directories below are versioned.  We only support the packages for the
# stable version of Octave
export mpath = usr/share/octave/packages
export bpath = usr/lib/$(shell dpkg-architecture -qDEB_HOST_MULTIARCH)/octave/packages
hosttype = $(shell octave-config -p CANONICAL_HOST_TYPE)
export version = $(shell octave-config -p VERSION | sed 's/-rc/~rc/')
apiversion = $(shell octave-config -p API_VERSION)
export octave = /usr/bin/octave-cli
export octave_options = --no-history --silent --no-init-file --no-window-system
export global_list = global-list
export local_list = local-list
libdir = /usr/share/octave-pkg-dev

# The following variable can be overriden in debian/rules to alter the
# behavior of the tests.  Possible values are 'quiet', 'normal', and
# 'verbose'.  Users must set its value after inclusion of octave-pkg.mk.
export OCTPKG_TEST_OPT =

# The following variable can be overriden in debian/rules to alter the the
# way the tests are run.  Its is value is prepended to the octave command.
# A sensible value for it would be 'xvfb-run', but environment variables
# can be also set.  Users must set its value after inclusion of
# octave-pkg.mk.
export OCTPKG_TEST_ENV =

# The following variable can be overriden in debian/rules to prevent
# the execution of the test blocks for certian files.  This variable
# should contain a space-separated list of file names (*.m or *.cc)
# for which the unit tests should not be run.
OCTPKG_EXCLUDE_TEST =

export OCTAVE_HISTFILE=/dev/null
# even with '--no-history', Octave creates an empty .octave_hist at startup,
# resulting in 'permission denied errors on the buildds

# Export buildflags to the environment, they will be picked up by mkoctfile.
# Link with --as-needed since that drastically reduces the number of linked
# libraries.
DEB_LDFLAGS_MAINT_APPEND=-Wl,--as-needed
DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/buildflags.mk
# Oct-files ldflags are taken from the following variable
export OCT_LINK_OPTS=$(LDFLAGS)

# Compose the expression for exclusion of requested files from the find command
export excluded_files_expr = $(shell for i in $(OCTPKG_EXCLUDE_TEST) ; do echo " -a ! -name $$i " ; done)

PACKAGES = $(shell perl -e 'use Debian::Debhelper::Dh_Lib;print join(" ", getpackages())')

%:
	dh $@

override_dh_auto_build:
	## The following is a hack that simplifies the switch from
	## CDBS into dh for packages using the building infrastructure
	## of octave-pkg-dev.
	-$(MAKE) -k -f debian/rules pre-build
	-for pkg in $(PACKAGES); do 			\
	    $(MAKE) -k -f debian/rules build/$$pkg ;	\
	done

override_dh_auto_install:
	# Check version
	$(libdir)/check-version
	# Create directories
	mkdir -p $(CURDIR)/$(debpkg)/$(mpath)	\
		 $(CURDIR)/$(debpkg)/$(bpath)
	# Install packages
	$(libdir)/install-pkg
	# Clean files
	( cd $$(find $(CURDIR)/$(debpkg) -name packinfo) ; 	\
	  rm -f COPYING ChangeLog )
	# Install documentation files
	$(libdir)/install-docs
ifeq (,$(findstring nocheck,$(DEB_BUILD_OPTIONS)))
	# Check package
	$(OCTPKG_CHECK_ENV) $(libdir)/check-pkg
endif
	# Remove directories
	-rmdir --ignore-fail-on-non-empty -p	\
		 $(CURDIR)/$(debpkg)/$(mpath)	\
		 $(CURDIR)/$(debpkg)/$(bpath)
	## The following is a hack that simplifies the switch from
	## CDBS into dh for packages using the building infrastructure
	## of octave-pkg-dev.
	-for pkg in $(PACKAGES); do 			\
	    $(MAKE) -k -f debian/rules install/$$pkg ;	\
	done

override_dh_auto_test:

DEB_MAKE_CLEAN_TARGET = -C src clean

override_dh_auto_clean:
ifneq ($(DEB_MAKE_CLEAN_TARGET),)
	-$(MAKE) -k $(DEB_MAKE_CLEAN_TARGET)
endif
	rm -rf inst/$(hosttype)-$(apiversion) $(global_list) $(local_list)
