#!/usr/bin/perl -w

use Parse::DebControl;
use Dpkg::Changelog;

my $parser = new Parse::DebControl;
my $data = $parser->parse_file ('./debian/control') -> [0];

for my $field ("Git", "Browser") {
    (my $tmp = $data -> {"Vcs-$field"}) =~ s/pkg-dev/\$name/;
    $data->{"Vcs-$field"} = $tmp;
}

open (FID, "< debian/compat");
$data->{Compat} = join ("", <FID>);
chomp $data->{Compat};
close FID;

open (FID, "< debian/changelog");
while (<FID>) {
    if (/\(([\d.]+)\)/) {
        $data->{Version} = $1;
        last;
    }
}

$base = "make-octave-forge-debpkg";
open (IN, "< $base.in");
open (OUT, "> $base");

while (<IN>) {
    for my $field ("Uploaders", "Standards-Version", "Maintainer",
                   "Vcs-Git", "Vcs-Browser", "Compat", "Version") {
        ## Escape @ signs in email addresses
        (my $value = $data->{$field}) =~ s/@/\\@/g;
        s{#$field#}{$value};
    }
    print OUT;
}

close IN;
close OUT;

