#!/bin/sh

# Set the octave executable and the octave options, when the
# corresponding variables are absent from the environment.
: ${octave:=octave-cli}
: ${octave_options:="--no-history --silent --no-init-file --no-window-system"}

[ -e PKG_ADD ] && mv PKG_ADD PKG_ADD.bak

echo Checking package...

unit_test_regex='^%!\(assert\|test\|error\|fail\|xtest\|warning\)'

# Extract tests from installed m files
tmp_script=$(tempfile)
tmp_results=$(tempfile)
cat >$tmp_script <<EOF
fid = fopen ("$tmp_results", "w");
disp ('echo Checking m files ...');
addpath (genpath (sprintf ('%s/debian', pwd ())));
[usr_pkg, sys_pkg] = pkg ('list');
for i = 1 : length (sys_pkg)
    pkg ('load', sys_pkg {1, i}.name);
endfor
EOF
for f in $(find inst/ -name \*.m $excluded_files_expr	| grep -v /private/) ; do
    if grep -q "$unit_test_regex" $f ; then
        stem=$(echo $f | sed 's:[^@]*/::;s/\.m$//')
	cat >>$tmp_script <<EOF
disp ("[$stem]");
[npass, ntest, nxfail, nskip] = test ("$stem",
                                      ifelse (strcmp ("$OCTPKG_TEST_OPT", ""),
                                              "verbose", "$OCTPKG_TEST_OPT"));
printf ("%d test%s, %d passed, %d known failure%s, %d skipped\n",
        ntest, ifelse (ntest > 1, "s", ""), npass, nxfail,
	ifelse (nxfail > 1, "s", ""), nskip);
fprintf (fid, "%s %d %d %d %d\n", "$stem", ntest, npass, nxfail, nskip);
EOF
    fi
done

# Extract tests from .cc files - these are not installed, but the
# compiled .oct files are.
# We search for the tests in the .cc files, but invoke the .oct files;
# this means we must add generate a loadpath starting at the current
# directory and source PKG_ADD files (they might add autoload()
# directives)

# We deactivate the warning about relative paths used for the PKG_ADD file.
cat >>$tmp_script <<EOF
disp ('echo Checking CC files ...');
addpath (genpath (pwd (), '.pc'));
[usr_pkg, sys_pkg] = pkg ('list');
for i = 1 : length (sys_pkg);
    pkg ('load', sys_pkg {1, i}.name);
endfor
warning ('off', 'Octave:autoload-relative-file-name');
EOF
if [ -f PKG_ADD ] ; then
    echo "source('PKG_ADD');" >> $tmp_script
fi
if [ -f PKG_ADD.bak ] ; then
    echo "source('PKG_ADD.bak');" > $tmp_script
fi
if [ -d src ] ; then
    for f in $(find src/ -name \*.cc $excluded_files_expr) ; do
        if grep -q "$unit_test_regex" $f ; then
            stem=${f%%.cc}
            stem=${stem##*/}
	cat >>$tmp_script <<EOF
disp ("[$stem]");
[npass, ntest, nxfail, nskip] = test ("$stem",
                                      ifelse (strcmp ("$OCTPKG_TEST_OPT", ""),
                                              "verbose", "$OCTPKG_TEST_OPT"));
printf ("%d test%s, %d passed, %d known failure%s, %d skipped\n",
        ntest, ifelse (ntest > 1, "s", ""), npass, nxfail,
	ifelse (nxfail > 1, "s", ""), nskip);
fprintf (fid, "%s %d %d %d %d\n", "$stem", ntest, npass, nxfail, nskip);
EOF
        fi
    done
    echo "fclose (fid);" >>$tmp_script
fi
$OCTPKG_TEST_ENV $octave $octave_options $tmp_script
rm -f $tmp_script

if [ -f debian/check.m ] ; then
    $OCTPKG_TEST_ENV $octave $octave_options --eval	\
        "addpath (genpath ([pwd(), '/debian']));	\
         [usr_pkg, sys_pkg] = pkg ('list');		\
         for i = 1 : length (sys_pkg);			\
           pkg ('load', sys_pkg {1, i}.name);		\
         endfor;					\
         source ('debian/check.m');"
fi

if [ -e PKG_ADD.bak ] ; then
    mv PKG_ADD.bak PKG_ADD
fi

ntest=0
npass=0
nxfail=0
nskip=0
while read f1 f2 f3 f4 f5 ; do
    ntest=$((ntest + f2))
    npass=$((npass + f3))
    nxfail=$((nxfail + f4))
    nskip=$((nskip + f5))
done < $tmp_results
rm -f $tmp_results
echo "Summary: $ntest tests, $npass passed, $nxfail known failures, $nskip skipped"
if [ "$ntest" -gt $((npass + nxfail + nskip)) ] ; then
    exit 1
fi
